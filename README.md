### NOTES
Random notebooks to test [fastai](https://github.com/fastai/fastai)

### using cpu deepo: ufoym/deepo:all-jupyter-py36-cpu
docker build -t py/fastai .
docker run -it --rm -p 8888:8888 -v "$PWD":/usr/src/app py/fastai bash
jupyter notebook --ip=0.0.0.0 --port=8888 --allow-root
